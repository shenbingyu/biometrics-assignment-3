file_path='/Users/bingyushen/Downloads/iris_dataset/';

matfile im_template(100);
matfile im_mask(100);
matfile im_filename(100);

file = dir(fullfile(file_path,'*.tiff'));

for i=1:100
    image_path=strcat(file_path,file(i).name);
    filename=cellstr(file(i).name);
    [template,mask]=createiristemplate(image_path);
    im_template{i}=template;
    im_mask{i}=mask;
    im_filename{i}=filename;
end

save('enrollmant.mat')

    
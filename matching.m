testfile=fopen('/Users/bingyushen/Desktop/genuine_impostor_pairs.txt');
template1=zeros(20,480);
template2=zeros(20,480);
mask1=zeros(20,480);
mask2=zeros(20,480);
distances=zeros(1,550);

for i=1:550
    filename=fgets(testfile);
    filename1=filename(1:20);
    filename2=filename(22:41);

    for j=1:100 
        if strcmp(im_filename{j},filename1);
            template1=im_template{j};
            mask1=im_mask{j};
        end
    end
    
    for k=1:100
        if strcmp(im_filename{k},filename2);
            template2=im_template{k};
            mask2=im_mask{k};
            
        end
    end
    
    distances(i)=gethammingdistance(template1,mask1,template2,mask2,1);
end

graw=distances(1:50);
iraw=distances(51:550);

genuine=sort(graw);
impostor=sort(iraw);

gmean=mean(genuine)
imean=mean(impostor)
stdg=std(genuine)
stdi=std(impostor)

genuinegraph=normpdf(genuine,gmean,stdg);
impostorgraph=normpdf(impostor,imean,stdi);

[g,gout]=hist(genuinegraph);
[i,iout]=hist(impostorgraph);

plot(genuine,genuinegraph,impostor,impostorgraph)

lowestgenuine1=graw(1);
highestgenuine1=graw(1);
l1=1;
h1=1;
for i=1:50
    if lowestgenuine1>graw(i);
        lowestgenuine1=graw(i);
        l1=i;
    else
        highestgenuine1=graw(i);
        h1=i;
    end
end

lowestgenuinepair=l1
lowestgenuine=lowestgenuine1
highestgenuinepair=h1
highestgenuine=highestgenuine1

lowestimpostor2=iraw(1);
highestimpostor2=iraw(1);
l2=1;
h2=1;
for i=1:500
    if lowestimpostor2>=iraw(i);
        lowestimpostor2=iraw(i);
        l2=i;
    else
        highestimpostor2=iraw(i);
        h2=i;
    end
end

lowestimpostorpair=l2
lowestimpostor=lowestimpostor2
highestimpostorpair=h2
highestimpostor=highestimpostor2
